<?php

/**
 * @copyright Adrian Borodziuk adrian.borodziuk@gmail.com
 */

class GreenIt_Notes_Adminhtml_NotesController extends Mage_Adminhtml_Controller_Action
{
    public function newAction()
    {
        $this->prepareNoteForm()->renderLayout();
    }
    
    public function editAction()
    {
        $this->prepareNoteForm()->renderLayout();
    }
    
    private function prepareNoteForm()
    {
        if($id = $this->getRequest()->getParam('id')) {
            Mage::register('current_note', Mage::getModel('greenit_notes/note')
                    ->load($id));
        }
        
        return $this->loadLayout()->_addContent(
                $this->getLayout()->createBlock('greenit_notes/adminhtml_notes_edit')
            );
    }
    
    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost()) {
            $model = Mage::getModel('greenit_notes/note');

            try {
                if ($id = $this->getRequest()->getParam('id')) {
                    $model->load((int)$id);
                }
                $model->addData($data);
                $model->save();
                
                Mage::getSingleton('adminhtml/session')
                    ->addSuccess('Note saved successfuly.');
                $this->_redirect('*/*/list'); 
            } catch (Exception $e){
                Mage::getSingleton('adminhtml/session')
                    ->addError('Not Saved. Error:'.$e->getMessage());
                $this->_redirect('*/*/list', array()); 
            }
        }    
    }
    
    public function deleteAction()
    {
        $id = $this->getRequest()->getParam('id');
        if(!$id) {
            $this->_redirect('*/*/list', array()); 
        }
        
        try {
            $model = Mage::getModel('greenit_notes/note')
                ->load((int)$id);
            $model->delete();

            Mage::getSingleton('adminhtml/session')
                ->addSuccess('Note removed successfuly.');
            $this->_redirect('*/*/list'); 
        } catch (Exception $e){
            Mage::getSingleton('adminhtml/session')
                ->addError('An error occured while removing note. Error:'.$e->getMessage());
            $this->_redirect('*/*/list', array()); 
        }
    }
    
    public function listAction()
    {
        $this->_title($this->__('Notes'))->_title($this->__('List'));
        $this->loadLayout();
        $this->_setActiveMenu('greenit_notes/notes');
        $this->_addContent($this->getLayout()->createBlock('greenit_notes/adminhtml_notes_list'));
        $this->renderLayout();
    }

    public function gridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('greenit_notes/adminhtml_notes_list_grid')->toHtml()
        );
    }
}

