<?php

/**
 * @copyright Adrian Borodziuk adrian.borodziuk@gmail.com
 */

class GreenIt_Notes_Block_Adminhtml_Notes_list
    extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_blockGroup = 'greenit_notes';
        $this->_controller = 'adminhtml_notes_list';
        $this->_headerText = Mage::helper('greenit_notes')->__('Notes');

        parent::__construct();
    }
} 

