<?php

/**
 * @copyright Adrian Borodziuk adrian.borodziuk@gmail.com
 */

class GreenIt_Notes_Block_Adminhtml_Notes_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form(array(
            'id' => 'edit_form',
            'action' => $this->getUrl('*/*/save', array('id' => $this->getRequest()->getParam('id'))),
            'method' => 'post',
            'enctype' => 'multipart/form-data'
        ));
        
        $fieldset = $form->addFieldset('notes_fieldset', array(
            'legend' => Mage::helper('greenit_notes')->__('Note information')
        ));

        $fieldset->addField('title', 'text', array(
            'label'     => Mage::helper('greenit_notes')->__('Title'),
            'class'     => 'required-entry',
            'required'  => true,
            'name'      => 'title',
        ));
        
        $fieldset->addField('content', 'textarea', array(
            'label'     => Mage::helper('greenit_notes')->__('Content'),
            'class'     => 'required-entry',           
            'required'  => true,
            'name'      => 'content',
        ));
        
        $form->setUseContainer(true);
        $this->setForm($form);
        
        $data = $this->_getFormData();
        if($data) {
            $form->addValues($data);
        }
        
        return parent::_prepareForm();
    }
    
    private function _getFormData()
    {
        if (Mage::registry('current_note')) {
            return Mage::registry('current_note')->getData();
        }
        return array();
    }
}

