<?php

/**
 * @copyright Adrian Borodziuk adrian.borodziuk@gmail.com
 */

class GreenIt_Notes_Block_Adminhtml_Notes_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();

        $this->_blockGroup = 'greenit_notes';
        $this->_mode = 'edit';
        $this->_controller = 'adminhtml_notes';
    }

    public function getHeaderText()
    {
        if (Mage::registry('current_note')) {
            $note = Mage::registry('current_note');
            return Mage::helper('greenit_notes')->__("Edit note: %s", $note->getTitle());
        }
        return Mage::helper('greenit_notes')->__("New note");
    }

    public function getBackUrl()
    {
        return $this->getUrl('*/*/list');
    }
}

