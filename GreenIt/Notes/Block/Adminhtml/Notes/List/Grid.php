<?php

/**
 * @copyright Adrian Borodziuk adrian.borodziuk@gmail.com
 */

class GreenIt_Notes_Block_Adminhtml_Notes_List_Grid 
    extends Mage_Adminhtml_Block_Widget_Grid
{
    
    public function __construct()
    {
        parent::__construct();
        $this->setId('greenit_notes_adminhtml_notes_list_grid');
        $this->setDefaultSort('note_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(false);
        $this->setUseAjax(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('greenit_notes/note')->getCollection();

        $this->setCollection($collection);
        parent::_prepareCollection();

        return $this;
    }

    protected function _prepareColumns()
    {
        $helper = Mage::helper('greenit_notes');
        
        $this->addColumn('note_id', array(
            'header' => $helper->__('Note #'),
            'index'  => 'note_id'
        ));

        $this->addColumn('title', array(
            'header' => $helper->__('Title'),
            'index'  => 'title'
        ));
        
        $this->addColumn('content', array(
            'header' => $helper->__('Content'),
            'index'  => 'content'
        ));

        $this->addColumn('created_time', array(
            'header' => $helper->__('Created at'),
            'type'   => 'datetime',
            'index'  => 'created_time'
        ));
        
        $this->addColumn('update_time', array(
            'header' => $helper->__('Last update'),
            'type'   => 'datetime',
            'index'  => 'update_time'
        ));

        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return $this->getUrl("adminhtml/notes/edit", array('id' => $row->getNoteId()));
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }
}

