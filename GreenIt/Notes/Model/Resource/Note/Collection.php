<?php

/**
 * @copyright Adrian Borodziuk adrian.borodziuk@gmail.com
 */

class GreenIt_Notes_Model_Resource_Note_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    public function _construct()
    {
        $this->_init('greenit_notes/note');
    }
} 
