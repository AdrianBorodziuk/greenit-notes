<?php

/**
 * @copyright Adrian Borodziuk adrian.borodziuk@gmail.com
 */

class GreenIt_Notes_Model_Note extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('greenit_notes/note');
    }
    
    protected function _beforeSave()
    {
        $timestamp = Mage::getModel('core/date')->date('Y-m-d H:i:s');
        if(!$this->getCreatedTime()) {
            $this->setCreatedTime($timestamp);
        }
        $this->setUpdateTime($timestamp);
        
        parent::_beforeSave();
    }
}

